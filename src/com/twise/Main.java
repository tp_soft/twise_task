package com.twise;



import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class Main {




    public static void main(String[] args) {
        int N = 100;
        ArrayList<Integer> list = new ArrayList<>();
        for(int i=1; i <=N; i ++){list.add(i);}

        int k = 0;
        for (int i = 1 ; i < N ; i++){
            list.remove(k);
            k += i;
            k %= list.size();
            System.out.println("Chairs now: " + list);
        }
        System.out.println("Chairs After: " + list);
        //Prints 31
        //Here the task is finalized!
    }
}
